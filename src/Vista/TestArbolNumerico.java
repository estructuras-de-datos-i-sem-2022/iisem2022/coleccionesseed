/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Negocio.ArbolBin_Numerico;
import colecciones.seed.ListaCD;
import util.BTreePrinter;

/**
 *
 * @author estudiante
 */
public class TestArbolNumerico {

    public static void main(String[] args) {
        ArbolBin_Numerico a = new ArbolBin_Numerico();
        System.out.println("Cardinalidad:" + a.getCardinalidad());
        System.out.println("Cantidad Nodos Ramas:" + a.getT().getCardinalidad_NodosRamas());
        System.out.println("Cantidad de Hojas izquierdas:" + a.getT().getCardinalidad_HojasIzq());
        System.out.println("Nodos Ramas:");
        imp(a.getT().getNodosRamas());
        System.out.println("PreOrden:");
        imp(a.getT().getPreOrden());
        System.out.println("inOrden:");
        imp(a.getT().getInOrden());
        System.out.println("PosOrden:");
        imp(a.getT().getPosOrden());
        System.out.println("Código Lukasiewcks:" + a.getT().getLuka());
        System.out.println("Recorrido por niveles:");
        imp(a.getT().getNiveles());
        BTreePrinter.printNode(a.getT().getRaiz());

        System.out.println("Recorrido por niveles:");
        System.out.println(a.getT().getNodosNiveles().toString());
        
        System.out.println("Recorrido ÚLTIMO nivel:");
        System.out.println(a.getT().getNodosUltimoNivel().toString());
        
         System.out.println("Recorrido Pre Orden_iterativo:");
        System.out.println(a.getT().getPreOrden_Iterativo().toString());
    }

    private static void imp(ListaCD<Integer> l) {
        for (Integer x : l) {
            System.out.print(x + "->");
        }
        System.out.println("");
    }

}
