/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import colecciones.seed.ColaP;

/**
 *
 * @author Estudiantes
 */
public class TestColaPrioridad {

    public static void main(String[] args) {
        ColaP<String> nombres = new ColaP();
        nombres.enColar("madarme", 30);
        nombres.enColar("jrangel", 25);
        nombres.enColar("ctovar", 40);
        while (!nombres.esVacia()) {
            System.out.println(nombres.deColar());
        }

    }
}
