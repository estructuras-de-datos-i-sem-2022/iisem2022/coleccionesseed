/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Modelo.Persona;
import colecciones.seed.ListaS;

/**
 *
 * @author madarme
 */
public class TestListaS {

    public static void main(String[] args) {
        ListaS<Integer> numeros = new ListaS();
        ListaS<Persona> personas = new ListaS();

        for (int i = 0; i < 10; i++) {
            numeros.addInicio(i);
            personas.addInicio(new Persona(i, "Nombre " + i));

        }

        for (int i = 94; i < 101; i++) {
            numeros.addFin(i);
            personas.addFin(new Persona(i, "Nombre " + i));

        }

        System.out.println(numeros.toString());
        System.out.println(personas.toString());

        int suma = 0;
        int x;
        for (int i = 0; i < numeros.getSize(); i++) {
            suma += numeros.get(i);
        }
        System.out.println("La suma de la lista de números es:"+suma);
        
        System.out.println("Borrando pos 3:"+numeros.remove(3));
        System.out.println("Lista queda:");
        System.out.println(numeros.toString());
        
        System.out.println("Borrando pos 0:"+numeros.remove(0));
        System.out.println("Lista queda:");
        System.out.println(numeros.toString());
        
        System.out.println("Cambiando pos 3 por su doble:");
        numeros.set(3, numeros.get(3)*2);
        System.out.println("Lista queda:");
        System.out.println(numeros.toString());
    }

}
