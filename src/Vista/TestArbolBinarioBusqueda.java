/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import colecciones.seed.ArbolBinarioBusqueda;
import util.BTreePrinter;

/**
 *
 * @author Estudiantes
 */
public class TestArbolBinarioBusqueda {

    public static void main(String[] args) {
        ArbolBinarioBusqueda<Integer> t = new ArbolBinarioBusqueda();
        System.out.println(t.insertar(100));
        System.out.println(t.insertar(110));
        System.out.println(t.insertar(90));
        System.out.println(t.insertar(190));
        System.out.println(t.insertar(105));
        BTreePrinter.printNode(t.getRaiz());
        System.out.println("Está el 105:" + t.esta(105));
        System.out.println("Está el 109:" + t.esta(109));
        System.out.println("El mayor dato del árbol es:" + t.getMayor());
        System.out.println("El mayor dato(recursivo) del árbol es:" + t.getMayor2());

    }
}
