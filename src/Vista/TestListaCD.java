/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import colecciones.seed.ListaCD;

/**
 *
 * @author madarme
 */
public class TestListaCD {
    public static void main(String[] args) {
        ListaCD<Integer> list = new ListaCD();
        list.addInicio(4);
        list.addInicio(5);
        list.addInicio(6);
        list.addInicio(7);
        System.out.println(list.toString());
        
        ListaCD<Integer> list2 = new ListaCD();
        list2.addFin(4);
        list2.addFin(5);
        list2.addFin(6);
        list2.addFin(7);
        System.out.println(list2.toString());
        
        System.out.println(list.remove(2));
        System.out.println(list.toString());
    }
    
}
