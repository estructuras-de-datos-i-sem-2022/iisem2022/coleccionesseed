/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import colecciones.seed.Pila;

/**
 *
 * @author auxiliarsb
 */
public class TestEvaluador {

    public static void main(String[] args) {
        String posfijo = "3,4,3,8,6,+,*,/,+,3,4,^,-";
        System.out.println("Resultado es:" + evaluar(posfijo));
    }

    private static float evaluar(String posfijo) {

        Pila<Float> aux = new Pila();
        String tokens[] = posfijo.split(",");
        
        for (String token : tokens) {
            if (!esSigno(token)) {
                aux.push(Float.parseFloat(token));
            } else {
                if (aux.getSize() < 2) {
                    throw new RuntimeException("La expresión en posfijo está mal escrita");
                    
                }
                
                aux.push(evaluar(aux.pop(),aux.pop(),token));

            }
        }
        return aux.pop();
    }

    private static float evaluar(float num2, float num1, String operador) {

        switch (operador) {
            case "+":
                return num1 + num2;
            case "-":
                return num1 - num2;
            case "*":
                return num1 * num2;
            case "/": {
                if(num2==0)
                    throw new RuntimeException("Operación inválida");
                return num1 / num2;
            }
            case "^":
                return (float) Math.pow(num1, num2);

        }
        return 0.0F;
    }

    private static boolean esSigno(String token) {
        return token.equals("+") || token.equals("*") || token.equals("/") || token.equals("-") || token.equals("^");
    }
}
