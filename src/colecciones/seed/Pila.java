/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colecciones.seed;

/**
 *
 * @author estudiante
 */
public class Pila <T>{
    private ListaCD <T> lista = new ListaCD();

    public Pila() {
        
    }
    
    public void push(T info){
        lista.addInicio(info);
    }
    
    public T pop (){
        if(this.lista.isVacia())
            throw new RuntimeException("La pila es vacia");
        return lista.remove(0);
    }
    
    public boolean esVacio(){
        return lista.isVacia();
    }
    
    public int getSize(){
        return lista.getSize();
    }
}
