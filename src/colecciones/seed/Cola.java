/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colecciones.seed;

/**
 *
 * @author estudiante
 */
public class Cola <T>{
   private ListaCD <T> lista = new ListaCD();

    public Cola() {
        
    }
    
    public void enColar(T info){
        lista.addFin(info);
    }
    
    public T deColar (){
        if(this.lista.isVacia())
            throw new RuntimeException("La pila es vacia");
        return lista.remove(0);
    }
    
    public boolean esVacio(){
        return lista.isVacia();
    }
    
    public int getSize(){
        return lista.getSize();
    } 
}
