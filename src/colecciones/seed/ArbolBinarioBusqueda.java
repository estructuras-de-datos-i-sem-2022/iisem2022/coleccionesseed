/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colecciones.seed;

/**
 *
 * @author Estudiantes
 */
public class ArbolBinarioBusqueda<T> extends ArbolBinario<T> {

    public ArbolBinarioBusqueda() {
        super();
    }

    public NodoBin<T> getRaiz() {
        return super.getRaiz();
    }

    public boolean insertar(T info) {
        if (info == null) {
            throw new RuntimeException("dato null");
        }

        boolean[] si = {true};
        NodoBin<T> raiznueva = insertar(this.getRaiz(), info, si);
        if (si[0]) {
            super.setRaiz(raiznueva);
        }

        return si[0];

    }

    private NodoBin<T> insertar(NodoBin<T> r, T elem, boolean[] si) {
        if (r == null) {
            return new NodoBin<T>(elem);
        } else {
            int c = ((Comparable) (r.getInfo())).compareTo(elem);
            //100-90 =10--> izq
            //100-110= -10 --> der
            //100 - 100 = 0 -> nada 
            if (c > 0) {
                r.setIzq(insertar(r.getIzq(), elem, si));
            } else {
                if (c < 0) {
                    r.setDer(insertar(r.getDer(), elem, si));
                } else {
                    si[0] = false;

                }
            }
        }
        return r;
    }

    public boolean esta(T elem) {
        if (super.esVacio() || elem == null) {
            throw new RuntimeException("dato null o bien árbol vacío");
        }
        return (esta(super.getRaiz(), elem));
    }

    private boolean esta(NodoBin<T> r, T elem) {
        //Condiciones base:
        if (r == null) {
            return false;
        }
        int c = ((Comparable) (r.getInfo())).compareTo(elem);
        if (c == 0) {
            return true;
        } else {
            if (c > 0) {
                return esta(r.getIzq(), elem);
            } else {
                return esta(r.getDer(), elem);
            }
        }

    }

    public T getMayor() {
        if (super.esVacio()) {
            throw new RuntimeException("árbol vacío");
        }
        NodoBin<T> r = super.getRaiz();
        for (; r.getDer() != null; r = r.getDer());
        return r.getInfo();
    }
    
    
     public T getMenor() {
        if (super.esVacio()) {
            throw new RuntimeException("árbol vacío");
        }
        NodoBin<T> r = super.getRaiz();
        for (; r.getIzq() != null; r = r.getIzq());
        return r.getInfo();
    }

    public T getMayor2() {
        if (super.esVacio()) {
            throw new RuntimeException("árbol vacío");
        }
        return getMayor(super.getRaiz());
    }

    private T getMayor(NodoBin<T> r) {
        if (r != null && r.getDer() == null) {
            return r.getInfo();
        }
        return (getMayor(r.getDer()));
    }
}
