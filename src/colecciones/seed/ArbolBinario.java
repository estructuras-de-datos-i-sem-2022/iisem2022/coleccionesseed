/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colecciones.seed;

/**
 *
 * @author estudiante
 */
public class ArbolBinario<T> {

    private NodoBin<T> raiz;

    public ArbolBinario() {
    }

    public NodoBin<T> getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoBin<T> raiz) {
        this.raiz = raiz;
    }

    public int getCardinalidad() {
        if (this.raiz == null) {
            throw new RuntimeException("No existe árbol");
        }
        return this.getCardinalidad(this.raiz);
    }

    private int getCardinalidad(NodoBin<T> r) {
        //Caso base:
        if (r == null) {
            return 0;
        }
        return (1 + getCardinalidad(r.getIzq()) + getCardinalidad(r.getDer()));
    }

    public boolean isHoja(NodoBin<T> r) {
        return r != this.raiz && r.getIzq() == null && r.getDer() == null;
    }

    public int getCardinalidad_NodosRamas() {
        if (this.raiz == null) {
            throw new RuntimeException("No existe árbol");
        }
        return this.getCardinalidad_NodosRamas(this.raiz);
    }

    private int getCardinalidad_NodosRamas(NodoBin<T> r) {
        //Caso base:
        if (r == null) {
            return 0;
        }
        int si = !this.isHoja(r) ? 1 : 0;

        return (si + getCardinalidad_NodosRamas(r.getIzq()) + getCardinalidad_NodosRamas(r.getDer()));
    }

    public int getCardinalidad_HojasIzq() {
        if (this.raiz == null) {
            throw new RuntimeException("No existe árbol");
        }
        return this.getCardinalidad_HojasIzq(this.raiz, false);
    }

    private int getCardinalidad_HojasIzq(NodoBin<T> r, boolean izq) {
        //Caso base:
        if (r == null) {
            return 0;
        }
        int si = this.isHoja(r) && izq ? 1 : 0;

        return (si + getCardinalidad_HojasIzq(r.getIzq(), true) + getCardinalidad_HojasIzq(r.getDer(), false));
    }

    public ListaCD<T> getNodosRamas() {
        if (this.raiz == null) {
            throw new RuntimeException("No existe árbol");
        }
        ListaCD<T> l = new ListaCD();
        getNodosRamas(this.raiz, l);
        return l;
    }

    private void getNodosRamas(NodoBin<T> r, ListaCD<T> l) {
        //Caso base:
        if (r == null) {
            return;
        }
        if (!this.isHoja(r)) {
            l.addFin(r.getInfo());
        }
        getNodosRamas(r.getIzq(), l);
        getNodosRamas(r.getDer(), l);

    }

    public ListaCD<T> getPreOrden() {
        if (this.raiz == null) {
            throw new RuntimeException("No existe árbol");
        }
        ListaCD<T> l = new ListaCD();
        getPreOrden(this.raiz, l);
        return l;
    }

    private void getPreOrden(NodoBin<T> r, ListaCD<T> l) {
        //Caso base:
        if (r == null) {
            return;
        }
        l.addFin(r.getInfo());
        getPreOrden(r.getIzq(), l);
        getPreOrden(r.getDer(), l);
    }

    public ListaCD<T> getInOrden() {
        if (this.raiz == null) {
            throw new RuntimeException("No existe árbol");
        }
        ListaCD<T> l = new ListaCD();
        getInOrden(this.raiz, l);
        return l;
    }

    private void getInOrden(NodoBin<T> r, ListaCD<T> l) {
        //Caso base:
        if (r == null) {
            return;
        }

        getInOrden(r.getIzq(), l);
        l.addFin(r.getInfo());
        getInOrden(r.getDer(), l);
    }

    public ListaCD<T> getPosOrden() {
        if (this.raiz == null) {
            throw new RuntimeException("No existe árbol");
        }
        ListaCD<T> l = new ListaCD();
        getPosOrden(this.raiz, l);
        return l;
    }

    private void getPosOrden(NodoBin<T> r, ListaCD<T> l) {
        //Caso base:
        if (r == null) {
            return;
        }

        getPosOrden(r.getIzq(), l);
        getPosOrden(r.getDer(), l);
        l.addFin(r.getInfo());
    }

    public String getLuka() {
        if (this.raiz == null) {
            throw new RuntimeException("No existe árbol");
        }
        return this.getLuka(this.raiz);
    }

    private String getLuka(NodoBin<T> r) {
        if (r == null) {
            return "b";
        }
        return ("a" + getLuka(r.getIzq()) + getLuka(r.getDer()));
    }

    public ListaCD<T> getNiveles() {
        if (this.raiz == null) {
            throw new RuntimeException("No existe árbol");
        }
        //Estructura de datos de direcciones de memoria
        Cola<NodoBin<T>> dir = new Cola();
        //Estructura de datos de elementos infos
        ListaCD<T> l = new ListaCD();
        dir.enColar(this.raiz);
        while (!dir.esVacio()) {
            NodoBin<T> dirTree = dir.deColar();
            l.addFin(dirTree.getInfo());
            if (dirTree.getIzq() != null) {
                dir.enColar(dirTree.getIzq());
            }

            if (dirTree.getDer() != null) {
                dir.enColar(dirTree.getDer());
            }
        }
        return l;
    }

    public ListaCD<String> getNodosNiveles() {
        if (this.raiz == null) {
            throw new RuntimeException("No existe el arbol");
        }
        int lvl = 0;
        Cola<Object[]> dirTree = new Cola();
        ListaCD<String> l = new ListaCD();

        dirTree.enColar(new Object[]{this.raiz, lvl});

        while (!dirTree.esVacio()) {
            Object temp[] = dirTree.deColar();
            NodoBin<T> r = (NodoBin) temp[0];
            lvl = (Integer) (temp[1]);
            if (r.getIzq() != null) {
                dirTree.enColar(new Object[]{r.getIzq(), lvl + 1});
            }
            if (r.getDer() != null) {
                dirTree.enColar(new Object[]{r.getDer(), lvl + 1});
            }

            l.addFin("(" + r.getInfo().toString() + " - " + lvl + ")");
        }
        return l;

    }

    public ListaCD<String> getNodosUltimoNivel() {
        if (this.raiz == null) {
            throw new RuntimeException("No existe el arbol");
        }
        int lvl = 0;
        Cola<Object[]> dirTree = new Cola();
        ColaP<Object[]> prioridad = new ColaP();
        ListaCD<String> l = new ListaCD();
        dirTree.enColar(new Object[]{this.raiz, lvl});

        while (!dirTree.esVacio()) {
            Object temp[] = dirTree.deColar();
            NodoBin<T> r = (NodoBin) temp[0];
            lvl = (Integer) (temp[1]);
            if (r.getIzq() != null) {
                dirTree.enColar(new Object[]{r.getIzq(), lvl + 1});
            }
            if (r.getDer() != null) {
                dirTree.enColar(new Object[]{r.getDer(), lvl + 1});
            }
            prioridad.enColar(temp, lvl);
        }
        Object aux[] = prioridad.deColar();
        NodoBin<T> r = (NodoBin) aux[0];
        lvl = (Integer) (aux[1]);
        l.addFin("(" + r.getInfo().toString() + " - " + lvl + ")");
        boolean flag = true;
        while (!prioridad.esVacia() && flag) {
            aux = prioridad.deColar();
            if ((Integer) (aux[1]) == lvl) {
                r = (NodoBin) (aux[0]);
                l.addFin("(" + r.getInfo().toString() + " - " + lvl + ")");
            } else {
                flag = !flag;
            }
        }
        return l;
    }

    public ListaCD<T> getPreOrden_Iterativo() {
        if (this.raiz == null) {
            throw new RuntimeException("No existe el arbol");
        }
        ListaCD<T> l = new ListaCD();
        Pila<NodoBin<T>> dirTree = new Pila();
        dirTree.push(raiz);
        NodoBin<T> r = null;
        while (!dirTree.esVacio()) {
            r = dirTree.pop();
            l.addFin(r.getInfo());
            if (r.getDer() != null) {
                dirTree.push(r.getDer());
            }
            if (r.getIzq() != null) {
                dirTree.push(r.getIzq());
            }

        }

        return l;
    }

    public boolean esVacio() {
        return this.raiz == null;
    }

    public ArbolBinario<T> crear(T pre[], T in[]) {
        return null;
    }

    public ListaCD<ListaCD<T>> getaRamaMasCorta() {
        return null;
    }

    public boolean esBinarioBusqueda() {
        return false;
    }
}
