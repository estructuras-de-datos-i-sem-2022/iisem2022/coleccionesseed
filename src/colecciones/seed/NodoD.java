/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package colecciones.seed;

/**
 *
 * @author madarme
 */
 class NodoD<T>{ 
    private T info;
    private NodoD sig,ant;

     NodoD() {
    }

     NodoD(T info, NodoD sig, NodoD ant) {
        this.info = info;
        this.sig = sig;
        this.ant = ant;
    }

     public T getInfo() {
        return info;
    }

     void setInfo(T info) {
        this.info = info;
    }

     NodoD getSig() {
        return sig;
    }

     void setSig(NodoD sig) {
        this.sig = sig;
    }

     NodoD getAnt() {
        return ant;
    }

     void setAnt(NodoD ant) {
        this.ant = ant;
    }
    
     
    
    
    
    
    
    
    
    
    
    
}




