/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Estudiante;
import Modelo.Semestre;
import colecciones.seed.ListaCD;
import util.ArchivoLeerURL;

/**
 *
 * @author estudiante
 */
public class SistemaAcademico {

    private Semestre[] semestres = new Semestre[10];

    public SistemaAcademico() {
    }

    public SistemaAcademico(String url) {
        ArchivoLeerURL a = new ArchivoLeerURL(url);
        Object v[] = a.leerArchivo();
        //codigos;nombre de estudiante;email;semestre
        this.crearSemestres();
        for (int i = 1; i < v.length; i++) {
            String datos[] = v[i].toString().split(";");
            int codigo = Integer.parseInt(datos[0]);
            String nombre = datos[1];
            String email = datos[2];
            byte sem = Byte.parseByte(datos[3]);
            Estudiante nuevo = new Estudiante(codigo, nombre, email);
            this.semestres[sem - 1].addEstudiante(nuevo);
        }
    }

    public void crearSemestres() {
        for (int i = 0; i < this.semestres.length; i++) {
            this.semestres[i] = new Semestre((byte) (i + 1));
        }

    }

    @Override
    public String toString() {
        String msg = "";
        for (Semestre s : this.semestres) {
            msg += s.toString() + "\n";
        }
        return msg;
    }

    /**
     * Obtiene una lista de estudiantes a partir de un semestre
     * @param sem un numero que representa el semestre
     * @return una lista de estudiantes de ese semestre
     */
    public ListaCD<Estudiante> getListaEstudiante(byte sem) {
        return null;
    }

    /**
     *  Obtiene un listado del id del semestre con más estudiantes matriculados
     * @return una cadena con los semestres
     */
    public String getSem_Con_Mas_Estudiantes() {
        return "el id de los semestres con mas estudiantes,separados por ,";
    }

    /**
     *  Verifica si un estudiante existe en el sistema
     * @param codigo un entero con el código del estudiantes
     * @return  true si el estudiante existe en el sistema o false en caso contrario
     */
    public boolean existeEstudiante(long codigo) {
        /**
         * método sobre la clase semestre
         */
        return false;
    }

    /**
     * Obtiene la información de un estudiante incluyendo su semestre
     * @param codigo un entero con el código del estudiantes
     * @return un String con la información del estudiante
     */
    public String getInfoEstudiante(long codigo) {
        // "Semestre+info del estudiante";
        /**
         * método sobre la clase semestre
         */
        return null;
    }

    /**
     * Obtiene un vector Ordenado por el número de estudiantes por semestre
     * de mayor a menor
     * utilice el método de inserción:
     * https://juncotic.com/ordenamiento-por-insercion-algoritmos-de-ordenamiento/
     * https://www.youtube.com/watch?v=nKzEJWbkPbQ
     * https://www.tutorialspoint.com/data_structures_algorithms/insertion_sort_algorithm.htm
     * @return un vector con objetos de la clase Semestre
     */
    public Semestre[] getSemestresOrdenados() {
        
        /**
         * método sobre está clase SistemaAcademico , pero
         * usa compareTo de Semestre
         */
        
        return null;
    }

    /**
     * Guarda en un pdf de nombre resultado.pdf
     * la información del toString de todos los semestres de forma:
     * 
     *      --------------------------------------------
     *      Semestre: 1
     *      Codigo |  Nombre | Email
     *      Codigo |  Nombre | Email
            --------------------------------------------
     *      Semestre: 2
     *      Codigo |  Nombre | Email
     *      Codigo |  Nombre | Email
     * 
     * 
     * 
     */
    public void guardarPDF()
    {
    
    }
    
}
