/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import colecciones.seed.ArbolBinario;
import colecciones.seed.NodoBin;

/**
 *
 * @author estudiante
 */
public class ArbolBin_Numerico {

    private ArbolBinario<Integer> t = new ArbolBinario();

    public ArbolBin_Numerico() {
        this.crear();
    }

    private void crear() {
        NodoBin<Integer> n1 = new NodoBin(2);
        NodoBin<Integer> n2 = new NodoBin(7);
        NodoBin<Integer> n3 = new NodoBin(5);
        NodoBin<Integer> n4 = new NodoBin(2);
        NodoBin<Integer> n5 = new NodoBin(6);
        NodoBin<Integer> n6 = new NodoBin(9);
        NodoBin<Integer> n7 = new NodoBin(5);
        NodoBin<Integer> n8 = new NodoBin(11);
        NodoBin<Integer> n9 = new NodoBin(4);
        this.t.setRaiz(n1);
        n1.setIzq(n2);
        n1.setDer(n3);
        n2.setIzq(n4);
        n2.setDer(n5);
        n3.setDer(n6);
        n5.setIzq(n7);
        n5.setDer(n8);
        n6.setIzq(n9);
    }

    public int getCardinalidad()
    {
        return this.t.getCardinalidad();
    }

    public ArbolBinario<Integer> getT() {
        return t;
    }
    
    
}
